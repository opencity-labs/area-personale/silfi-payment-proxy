package server

import (
	"encoding/json"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/opencontent/stanza-del-cittadino/silfi-payment-proxy/server/models"
)

type FlowEventStoreExternalPayment struct {
	Name          string
	Sctx          *ServerContext
	Event         *kafka.Message
	Tenant        *models.Tenant
	Service       *models.Service
	StoredPayment *models.Payment
	Err           error
	Msg           string
	Status        bool
	Result        *models.Payment
}

func (flow *FlowEventStoreExternalPayment) Exec() bool {
	flow.Name = "FlowEventStoreExternalPayment"

	flow.Status = true &&
		flow.extractPayment() &&
		flow.loadRelatedTenant() &&
		flow.loadRelatedService() &&
		flow.checkPrecondition() &&
		flow.checkStorePayment() &&
		flow.updatePayment() &&
		flow.storeExternalPayment() &&
		flow.produceEvent()

	if flow.Status {
		MetricsPaymentsReceived.Inc()
	}

	return flow.Status
}

func (flow *FlowEventStoreExternalPayment) extractPayment() bool {
	eventValue := flow.Event.Value
	flow.Result = &models.Payment{}
	err := json.Unmarshal(eventValue, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	if flow.Result.EventVersion != "2.0" {
		flow.Err = err
		flow.Msg = "unsupported event version"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	if flow.Result.TenantID == "" || flow.Result.ServiceID == "" {
		flow.Err = err
		flow.Msg = "missing tenant_id or service_id"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	return true
}

func (flow *FlowEventStoreExternalPayment) loadRelatedTenant() bool {
	tenantsCache := flow.Sctx.TenantsCache()

	tenant, err := tenantsCache.Get(flow.Sctx.Ctx(), flow.Result.TenantID)
	flow.Tenant = tenant
	if err != nil && err.Error() == "value not found in store" {
		flow.Err = err
		flow.Msg = "irrilevant tenant"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get tenant by id error for: " + flow.Result.TenantID

		MetricsPaymentsInternalError.Inc()

		return false
	}

	return true
}

func (flow *FlowEventStoreExternalPayment) loadRelatedService() bool {
	servicesCache := flow.Sctx.ServicesCache()

	service, err := servicesCache.Get(flow.Sctx.Ctx(), flow.Result.ServiceID)
	flow.Service = service
	if err != nil && err.Error() == "value not found in store" {
		flow.Err = err
		flow.Msg = "irrilevant service"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get service by id error for: " + flow.Result.ServiceID

		MetricsPaymentsInternalError.Inc()

		return false
	}

	return true
}

func (flow *FlowEventStoreExternalPayment) checkPrecondition() bool {
	if flow.Result.TenantID != flow.Service.TenantID {
		flow.Msg = "wrong tenant for this service"
		return false
	}
	if !flow.Tenant.Active {
		flow.Msg = "tenant not active"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	if !flow.Service.Active {
		flow.Msg = "service not active"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	if flow.Result.ID == "" {
		flow.Msg = "missing id"

		MetricsPaymentsValidationError.Inc()

		return false
	}
	if flow.Result.Status != "PAYMENT_PENDING" {
		flow.Msg = "irrilevant payment, status not in payment pending"
		return false
	}
	return true
}

// todoo da modificare
func (flow *FlowEventStoreExternalPayment) checkStorePayment() bool {
	paymentsCache := flow.Sctx.PaymentsCache()
	storedPayment, err := paymentsCache.Get(flow.Sctx.Ctx(), flow.Result.ID)
	flow.StoredPayment = storedPayment
	if err != nil && (err.Error() == "value not found in store" || err.Error() == "not found") {
		flow.Msg = "new payment found"
		MetricsPaymentsNew.Inc()

		return true
	}
	if err != nil && err.Error() != "value not found in store" && err.Error() != "not found" {
		flow.Msg = "get payment by id error for: " + flow.Result.ID

		MetricsPaymentsInternalError.Inc()

		return false
	}

	return false
}

func (flow *FlowEventStoreExternalPayment) updatePayment() bool {
	serverConfig := flow.Sctx.ServerConfig()
	now := models.TimeNow()
	flow.Result.Links.Update.Url = serverConfig.InternalApiUrl + serverConfig.BasePath + "update/" + flow.Result.ID
	flow.Result.Links.Update.LastCheckAt = now
	flow.Result.Links.Update.NextCheckAt = flow.Result.NextCheck()
	flow.Result.Links.Update.Method = "GET"

	flow.Result.UpdatedAt = now
	flow.Result.EventID = uuid.NewV4().String()
	flow.Result.EventCreatedAt = now
	flow.Result.AppID = serverConfig.AppName + ":" + VERSION

	return true
}

func (flow *FlowEventStoreExternalPayment) storeExternalPayment() bool {
	err := StorePayment(flow.Sctx, flow.Result)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "storage error"
		return false
	}
	return true
}

func (flow *FlowEventStoreExternalPayment) produceEvent() bool {
	err := ProducePayment(flow.Sctx, flow.Result)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"

		MetricsPaymentsInternalError.Inc()

		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "kafka producer error"

		MetricsPaymentsInternalError.Inc()

		return false
	}

	return true
}
