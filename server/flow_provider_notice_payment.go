package server

import (
	"bytes"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"io"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/silfi-payment-proxy/server/models"
)

type FlowProviderNoticePayment struct {
	Name     string
	Sctx     *ServerContext
	Payment  *models.Payment
	Service  *models.Service
	Tenant   *models.Tenant
	Request  *http.Request
	Client   *http.Client
	Err      error
	Msg      string
	Status   bool
	Result   *models.SilfiNoticeResponse
	Received bool
	Notice   []byte
}

func (flow *FlowProviderNoticePayment) Exec() bool {
	flow.Name = "ProviderNoticePayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderNoticePayment) prepareRequest() bool {
	apiEndpoint := flow.Tenant.EndpointBe + "/pagamentoAttesoPdfByIuv" + "?codiceEnte=" + flow.Tenant.Code + "&codiceServizio=" + flow.Service.Code + "&iuv=" + flow.Payment.Payment.IUV
	verifyReq, err := http.NewRequest("GET", apiEndpoint, bytes.NewReader([]byte{}))
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing verify request"
		return false
	}
	verifyReq.Header.Add("Content-Type", "application/json")

	flow.Request = verifyReq

	return true
}

func (flow *FlowProviderNoticePayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_notice"))

	flow.Client = &http.Client{}
	if flow.Tenant.SslCrt != "" && flow.Tenant.SslKey != "" {
		crt, err := tls.X509KeyPair([]byte(flow.Tenant.SslCrt), []byte(flow.Tenant.SslKey))
		if err != nil {
			flow.Err = err
			flow.Msg = "errors while loading the ssl certificate"

			MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

			return false
		}

		tlsConfig := &tls.Config{
			Certificates: []tls.Certificate{crt},
		}

		tr := &http.Transport{
			TLSClientConfig: tlsConfig,
		}
		flow.Client.Transport = tr
	}

	noticeRes, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting notice request"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}
	defer noticeRes.Body.Close()
	noticeBody, err := io.ReadAll(noticeRes.Body)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while reading notice response"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	flow.Result = &models.SilfiNoticeResponse{}
	err = json.Unmarshal(noticeBody, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error while reading notice response"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	if flow.Result.Notice == "" {
		flow.Msg = "provider response empty"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.Notice, err = base64.StdEncoding.DecodeString(flow.Result.Notice)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while decoding notice"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	flow.Received = true

	return true
}
