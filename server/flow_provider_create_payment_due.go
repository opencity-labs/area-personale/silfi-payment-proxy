package server

import (
	"bytes"
	"crypto/sha256"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/silfi-payment-proxy/server/models"
)

type FlowProviderCreatePaymentDue struct {
	Name          string
	Sctx          *ServerContext
	Payment       *models.Payment
	Service       *models.Service
	Tenant        *models.Tenant
	Request       *http.Request
	Client        *http.Client
	Err           error
	Msg           string
	Status        bool
	Result        *models.SilfiDuePagamentiAttesiResponse
	IUV           string
	NoticeCode    string
	TransactionID string
}

func (flow *FlowProviderCreatePaymentDue) Exec() bool {
	flow.Name = "ProviderCreatePaymentDue"

	flow.Status = true &&
		flow.selectPaymentRequestType()

	return flow.Status
}

func (flow *FlowProviderCreatePaymentDue) selectPaymentRequestType() bool {
	if flow.Service.Code == "marc_bollo" || flow.Service.Code == "bollo_open" {
		return flow.prepareMarcaDaBolloRequest() &&
			flow.doMarcaDaBolloRequest()
	}
	return true && flow.preparePayment() &&
		flow.preparePagamentiAttesiRequest() && //questo va fatto per prima oppure dopo
		flow.doPagamentiAttesiRequest()
}
func (flow *FlowProviderCreatePaymentDue) prepareMarcaDaBolloRequest() bool {
	payment := flow.Payment
	tenant := flow.Tenant
	config := flow.Sctx.serverConfig
	marcaDaBolloRequestPayload := models.MarcaDaBolloPayment{}
	var anagNaturaGiuridica string
	if flow.Payment.Payer.Type == "human" {
		anagNaturaGiuridica = "F"
	} else {
		anagNaturaGiuridica = "G"
	}

	paymentJson, err := json.Marshal(payment)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing due request for marca da bollo"
		return false
	}
	hash := sha256.Sum256(paymentJson)
	hashDocumento := base64.StdEncoding.EncodeToString(hash[:])

	marcaDaBolloRequestPayload.CodiceEnte = tenant.Code
	marcaDaBolloRequestPayload.Importo = payment.Payment.Amount
	marcaDaBolloRequestPayload.HashDocumento = hashDocumento
	marcaDaBolloRequestPayload.AnagDenominazione = payment.Payer.Name + " " + payment.Payer.FamilyName
	marcaDaBolloRequestPayload.AnagNaturaGiuridica = anagNaturaGiuridica
	marcaDaBolloRequestPayload.AnagProvincia = payment.Payer.CountrySubdivision
	marcaDaBolloRequestPayload.AnagCfPiva = payment.Payer.TaxIdentificationNumber
	marcaDaBolloRequestPayload.UrlRitorno = config.ExternalApiUrl + config.BasePath + "landing/" + payment.ID
	marcaDaBolloRequestPayload.CodiceServizio = flow.Service.Code

	dueDataRequestJson, err := json.Marshal(marcaDaBolloRequestPayload)
	if err != nil {
		flow.Err = err
		flow.Msg = "json marshal error while preparing due request for marca da bollo"
		return false
	}
	apiEndpoint := tenant.EndpointBe + "/marca-bollo/token-custom"
	dueReq, err := http.NewRequest("POST", apiEndpoint, bytes.NewReader(dueDataRequestJson))

	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing due request for marca da bollo"
		return false
	}
	dueReq.Header.Add("Content-Type", "application/json")

	flow.Request = dueReq

	return true
}

func (flow *FlowProviderCreatePaymentDue) doMarcaDaBolloRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("create_payment_due"))
	flow.Client = &http.Client{}
	if flow.Tenant.SslCrt != "" && flow.Tenant.SslKey != "" {
		crt, err := tls.X509KeyPair([]byte(flow.Tenant.SslCrt), []byte(flow.Tenant.SslKey))
		if err != nil {
			flow.Err = err
			flow.Msg = "errors while loading the ssl certificate"

			MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

			return false
		}

		tlsConfig := &tls.Config{
			Certificates: []tls.Certificate{crt},
		}

		tr := &http.Transport{
			TLSClientConfig: tlsConfig,
		}
		flow.Client.Transport = tr
	}

	dueRes, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting due request"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}
	defer dueRes.Body.Close()
	dueBody, err := io.ReadAll(dueRes.Body)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while reading due response"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	marcaDaBolloResponse := &models.SilfiDueMarcaDaBolloResponse{}
	err = json.Unmarshal(dueBody, marcaDaBolloResponse)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error while reading due response"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	if marcaDaBolloResponse.Esito != "OK" {
		flow.Err = err
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	if marcaDaBolloResponse.Iuv == "" {
		flow.Err = err
		flow.Msg = "provider did not give us a IUV for payment"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.IUV = marcaDaBolloResponse.Iuv
	flow.NoticeCode = "3" + marcaDaBolloResponse.Iuv

	return true
}

func (flow *FlowProviderCreatePaymentDue) preparePayment() bool {
	paymentDetailSplit := []*models.PaymentDetailSplit{}

	if flow.Service.Splitted && flow.Service.Split != nil {
		// fmt.Println("AAAA")
		// aaaa, _ := json.Marshal(flow.Service.Split)
		// fmt.Println(string(aaaa))

		for _, serviceSplit := range flow.Service.Split {
			serviceSplitAmount := serviceSplit.Amount

			paymentSplit := &models.PaymentDetailSplit{
				Code:   serviceSplit.Code,
				Amount: &serviceSplitAmount,
				Meta: &models.PaymentDetailSplitMeta{
					Description: serviceSplit.Description,
					AccEntry:    serviceSplit.AccEntry,
					AccCap:      serviceSplit.AccCap,
					AccArt:      serviceSplit.AccArt,
				},
			}
			paymentDetailSplit = append(paymentDetailSplit, paymentSplit)
		}
	}

	// fmt.Println("BBBB")
	// bbbb, _ := json.Marshal(paymentDetailSplit)
	// fmt.Println(string(bbbb))

	if flow.Payment.Payment.Split != nil {
		for _, incomingPaymentSplit := range flow.Payment.Payment.Split {
			if incomingPaymentSplit.Amount == nil {
				for index, paymentSplit := range paymentDetailSplit {
					if incomingPaymentSplit.Code == paymentSplit.Code {
						paymentDetailSplit = append(paymentDetailSplit[:index], paymentDetailSplit[index+1:]...)
					}
				}
			} else {
				for _, paymentSplit := range paymentDetailSplit {
					if incomingPaymentSplit.Code == paymentSplit.Code {
						paymentSplit.Amount = incomingPaymentSplit.Amount
					}
				}
			}
		}
	}

	// fmt.Println("CCCC")
	// cccc, _ := json.Marshal(paymentDetailSplit)
	// fmt.Println(string(cccc))
	// fmt.Println("DDDD")
	// dddd, _ := json.Marshal(flow.Payment.Payment.Split)
	// fmt.Println(string(dddd))

	flow.Payment.Payment.Split = paymentDetailSplit

	var totalAmount float64 = 0.0
	for _, paymentSplit := range flow.Payment.Payment.Split {
		totalAmount += *paymentSplit.Amount
	}

	// fmt.Println("EEEE")
	// eeee, _ := json.Marshal(flow.Payment.Payment.Split)
	// fmt.Println(string(eeee))

	if len(flow.Payment.Payment.Split) > 0 && flow.Payment.Payment.Amount != totalAmount {
		flow.Msg = "total amount and split amounts sum mismatching"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	return true
}

func (flow *FlowProviderCreatePaymentDue) preparePagamentiAttesiRequest() bool {
	var payerType, payerName string
	if flow.Payment.Payer.Type == "human" {
		payerType = "F"
		payerName = flow.Payment.Payer.Name + " " + flow.Payment.Payer.FamilyName
	} else {
		payerType = "G"
		payerName = flow.Payment.Payer.Name + " " + flow.Payment.Payer.FamilyName // manca ragione sociale?
	}

	dueSplits := []*models.SilfiDueRequestSplit{}

	if flow.Payment.Payment.Split != nil && len(flow.Payment.Payment.Split) != 0 {
		for _, paymentSplit := range flow.Payment.Payment.Split {

			dueSplit := &models.SilfiDueRequestSplit{
				Amount:      *paymentSplit.Amount,
				Description: paymentSplit.Meta.Description,
				AccCap:      paymentSplit.Meta.AccCap,
				AccArt:      paymentSplit.Meta.AccArt,
			}
			if paymentSplit.Meta.AccEntry != "" {
				dueSplit.AccEntry = &paymentSplit.Meta.AccEntry
			}
			dueSplits = append(dueSplits, dueSplit)
		}
	} else {
		dueSplit := &models.SilfiDueRequestSplit{
			Amount:      flow.Payment.Payment.Amount,
			Description: flow.Service.Description,
			AccCap:      flow.Service.AccCap,
			AccArt:      flow.Service.AccArt,
		}
		if flow.Service.AccEntry != "" {
			dueSplit.AccEntry = &flow.Service.AccEntry
		}
		dueSplits = append(dueSplits, dueSplit)
	}

	dueDataRequest := models.SilfiDueRequest{
		{
			Payment: &models.SilfiDueRequestPayment{
				TenantCode:  flow.Tenant.Code,
				ServiceCode: flow.Service.Code,
				PaymentId:   flow.Payment.Payment.IUD,
				Reason:      flow.Payment.Reason,
				Amount:      flow.Payment.Payment.Amount,
				Debits: []*models.SilfiDueRequestDebit{
					{
						DebitId:     flow.Payment.Payment.IUD,
						Amount:      flow.Payment.Payment.Amount,
						Description: flow.Service.Description,
						Splits:      dueSplits,
					},
				},
				PayerType:                    payerType,
				PayerTaxIdentificationNumber: flow.Payment.Payer.TaxIdentificationNumber,
				PayerName:                    payerName,
				PayerStreetName:              flow.Payment.Payer.StreetName,
				PayerBuildingNumber:          flow.Payment.Payer.BuildingNumber,
				PayerTownName:                flow.Payment.Payer.TownName,
				PayerPostalCode:              flow.Payment.Payer.PostalCode,
				PayerCountrySubdivision:      flow.Payment.Payer.CountrySubdivision,
				PayerCountry:                 flow.Payment.Payer.Country,
				PayerEmail:                   flow.Payment.Payer.Email,
				Visible:                      true,
				CreatedAt:                    flow.Payment.CreatedAt.Format("2006-01-02T15:04:05Z"),
				ExpireAt:                     flow.Payment.Payment.ExpireAt.Format("2006-01-02T15:04:05Z"),
				PrintExpireAt:                flow.Payment.Payment.ExpireAt.Format("2006-01-02T15:04:05Z"),
			},
			EntryCode: flow.Service.EntryCode,
		},
	}

	dueDataRequestJson, err := json.Marshal(dueDataRequest)
	if err != nil {
		flow.Err = err
		flow.Msg = "json marshal error while preparing due request"
		return false
	}
	apiEndpoint := flow.Tenant.EndpointBe + "/pagamentiAttesi"
	dueReq, err := http.NewRequest("POST", apiEndpoint, bytes.NewReader(dueDataRequestJson))

	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing due request"
		return false
	}
	dueReq.Header.Add("Content-Type", "application/json")

	flow.Request = dueReq

	return true
}

func (flow *FlowProviderCreatePaymentDue) doPagamentiAttesiRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("create_payment_due"))

	flow.Client = &http.Client{}
	if flow.Tenant.SslCrt != "" && flow.Tenant.SslKey != "" {
		crt, err := tls.X509KeyPair([]byte(flow.Tenant.SslCrt), []byte(flow.Tenant.SslKey))
		if err != nil {
			flow.Err = err
			flow.Msg = "errors while loading the ssl certificate"

			MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

			return false
		}

		tlsConfig := &tls.Config{
			Certificates: []tls.Certificate{crt},
		}

		tr := &http.Transport{
			TLSClientConfig: tlsConfig,
		}
		flow.Client.Transport = tr
	}
	dueRes, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting due request"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}
	defer dueRes.Body.Close()
	dueBody, err := io.ReadAll(dueRes.Body)
	fmt.Println("12345 risposta: " + string(dueBody))
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while reading due response"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	flow.Result = &models.SilfiDuePagamentiAttesiResponse{}
	err = json.Unmarshal(dueBody, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error while reading due response"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	if len(flow.Result.Payments) == 0 {
		flow.Err = err
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	result := flow.Result.Payments[0]
	if result.Result != "SUCCESS" || result.Message != "Operazione eseguita correttamente" {
		flow.Err = err
		flow.Msg = "provider response ko" //forse questo deve essere ok anziché ko???

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	if result.IUV == "" {
		flow.Err = err
		flow.Msg = "provider did not give us a IUV for payment"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.IUV = result.IUV
	flow.NoticeCode = "3" + result.IUV

	return true
}
