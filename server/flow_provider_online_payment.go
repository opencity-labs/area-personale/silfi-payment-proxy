package server

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"io"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/silfi-payment-proxy/server/models"
)

type FlowProviderOnlinePayment struct {
	Name    string
	Sctx    *ServerContext
	Payment *models.Payment
	Service *models.Service
	Tenant  *models.Tenant
	Request *http.Request
	Client  *http.Client
	Err     error
	Msg     string
	Status  bool
	Result  *models.SilfiOnlineResponse
	Url     string
}

func (flow *FlowProviderOnlinePayment) Exec() bool {
	flow.Name = "ProviderOnlinePayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderOnlinePayment) prepareRequest() bool {
	serverConfig := flow.Sctx.ServerConfig()

	iuvCode := flow.Payment.Payment.IUV

	onlineDataRequest := models.SilfiOnlineRequest{
		TenantCode:  flow.Tenant.Code,
		ServiceCode: flow.Service.Code,
		IUVs:        []string{iuvCode},
		LandingUrl:  serverConfig.ExternalApiUrl + serverConfig.BasePath + "landing/" + flow.Payment.ID,
	}

	onlineDataRequestJson, err := json.Marshal(onlineDataRequest)
	if err != nil {
		flow.Err = err
		flow.Msg = "json marshal error while preparing online request"
		return false
	}

	apiEndpoint := flow.Tenant.EndpointBe + "/token"
	onlineReq, err := http.NewRequest("POST", apiEndpoint, bytes.NewReader(onlineDataRequestJson))
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing online request"
		return false
	}
	onlineReq.Header.Add("Content-Type", "application/json")

	flow.Request = onlineReq

	return true
}

func (flow *FlowProviderOnlinePayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_online"))

	flow.Client = &http.Client{}
	if flow.Tenant.SslCrt != "" && flow.Tenant.SslKey != "" {
		crt, err := tls.X509KeyPair([]byte(flow.Tenant.SslCrt), []byte(flow.Tenant.SslKey))
		if err != nil {
			flow.Err = err
			flow.Msg = "errors while loading the ssl certificate"

			MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

			return false
		}
		tlsConfig := &tls.Config{
			Certificates: []tls.Certificate{crt},
		}

		tr := &http.Transport{
			TLSClientConfig: tlsConfig,
		}
		flow.Client.Transport = tr
	}
	onlineRes, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting online request"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}
	defer onlineRes.Body.Close()
	onlineBody, err := io.ReadAll(onlineRes.Body)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while reading online response"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}

	flow.Result = &models.SilfiOnlineResponse{}
	err = json.Unmarshal(onlineBody, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error while reading online response"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}

	if flow.Result.Token == "" {
		flow.Msg = "provider did not give us a URL for payment"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.Url = flow.Tenant.EndpointFe + "?token=" + flow.Result.Token + "&codiceEnte=" + flow.Tenant.Code + "&codiceServizio=" + flow.Service.Code + "&reloadSession=1"

	return true
}
