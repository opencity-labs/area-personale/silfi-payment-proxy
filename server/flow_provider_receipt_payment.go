package server

import (
	"bytes"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"io"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/silfi-payment-proxy/server/models"
)

type FlowProviderReceiptPayment struct {
	Name     string
	Sctx     *ServerContext
	Payment  *models.Payment
	Service  *models.Service
	Tenant   *models.Tenant
	Request  *http.Request
	Client   *http.Client
	Err      error
	Msg      string
	Status   bool
	Result   *models.SilfiReceiptResponse
	Received bool
	Receipt  []byte
}

func (flow *FlowProviderReceiptPayment) Exec() bool {
	flow.Name = "ProviderReceiptPayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderReceiptPayment) prepareRequest() bool {
	apiEndpoint := flow.Tenant.EndpointBe + "/ricevutaTelematicaByIuv" + "?codiceEnte=" + flow.Tenant.Code + "&codiceServizio=" + flow.Service.Code + "&iuv=" + flow.Payment.Payment.IUV + "&pdf=true"
	receiptReq, err := http.NewRequest("GET", apiEndpoint, bytes.NewReader([]byte{}))
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing receipt request"
		return false
	}
	receiptReq.Header.Add("Content-Type", "application/json")

	flow.Request = receiptReq

	return true
}

func (flow *FlowProviderReceiptPayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_receipt"))

	flow.Client = &http.Client{}
	if flow.Tenant.SslCrt != "" && flow.Tenant.SslKey != "" {
		crt, err := tls.X509KeyPair([]byte(flow.Tenant.SslCrt), []byte(flow.Tenant.SslKey))
		if err != nil {
			flow.Err = err
			flow.Msg = "errors while loading the ssl certificate"

			MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

			return false
		}

		tlsConfig := &tls.Config{
			Certificates: []tls.Certificate{crt},
		}

		tr := &http.Transport{
			TLSClientConfig: tlsConfig,
		}
		flow.Client.Transport = tr
	}
	receiptRes, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting receipt request"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}
	defer receiptRes.Body.Close()
	receiptBody, err := io.ReadAll(receiptRes.Body)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while reading receipt response"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	flow.Result = &models.SilfiReceiptResponse{}
	err = json.Unmarshal(receiptBody, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error while reading receipt response"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	if flow.Result.IUV != flow.Payment.Payment.IUV || flow.Result.Receipt == nil || flow.Result.Receipt.Attachment == nil {
		flow.Err = err
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	receipt := flow.Result.Receipt.Attachment.Receipt

	if receipt == "" {
		flow.Msg = "provider response empty"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.Receipt, err = base64.StdEncoding.DecodeString(receipt)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while decoding notice"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	flow.Received = true

	return true
}
