package server

import (
	"fmt"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/opencontent/stanza-del-cittadino/silfi-payment-proxy/server/models"
)

type FlowRequestPaymentNotify struct {
	Name                     string
	Sctx                     *ServerContext
	PaymentID                string
	Tenant                   *models.Tenant
	Service                  *models.Service
	PaymentNotificationInput []PaymentNotificationInput
	Err                      error
	Msg                      string
	Status                   bool
	Result                   *models.Payment
	Notice                   []byte
}

func (flow *FlowRequestPaymentNotify) Exec() bool {
	flow.Name = "RequestPaymentNotify"

	flow.Status = true &&
		flow.findPayment() &&
		flow.updatePayment()

	return flow.Status
}

func (flow *FlowRequestPaymentNotify) findPayment() bool {
	payment, err := GetpaymentByIuv(flow.Sctx, flow.PaymentNotificationInput[0].Results[0].IUV)
	flow.Result = payment
	if err != nil && (err.Error() == "value not found in store" || err.Error() == "not found") {
		flow.Err = err
		flow.Msg = "payment not found"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get payment by id error for: " + flow.PaymentNotificationInput[0].Results[0].IUV
		return false
	}

	return true
}

func (flow *FlowRequestPaymentNotify) updatePayment() bool {
	serverConfig := flow.Sctx.ServerConfig()
	paidAt, err := models.StringToTime(flow.PaymentNotificationInput[0].Results[0].PaymentDate)
	if err != nil {
		flow.Err = err
		flow.Msg = "error parsing dataPagamento into Time struct"
		return false
	}
	now, err := models.GetCurrentDateTime()
	if err != nil {
		flow.Err = err
		flow.Msg = "error parsing currentDateTime into Time struct"
		return false
	}
	fmt.Println("paidAt " + paidAt.String())

	flow.Result.UpdatedAt = now
	flow.Result.EventID = uuid.NewV4().String()
	flow.Result.EventCreatedAt = now
	flow.Result.EventVersion = "2.0"
	flow.Result.AppID = serverConfig.AppName + ":" + VERSION
	flow.Result.Status = "COMPLETE"
	flow.Result.Payment.PaidAt = paidAt
	err = StorePayment(flow.Sctx, flow.Result)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "storage error"
		return false
	}

	err = ProducePayment(flow.Sctx, flow.Result)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "kafka producer error"
		return false
	}

	return true
}
