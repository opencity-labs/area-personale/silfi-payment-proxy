package server

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"io"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/silfi-payment-proxy/server/models"
)

type FlowProviderVerifyPayment struct {
	Name      string
	Sctx      *ServerContext
	Payment   *models.Payment
	Service   *models.Service
	Tenant    *models.Tenant
	Request   *http.Request
	Client    *http.Client
	Err       error
	Msg       string
	Status    bool
	Result    *models.SilfiVerifyResponse
	Completed bool
	Payed     bool
	PaidAt    models.Time
}

func (flow *FlowProviderVerifyPayment) Exec() bool {
	flow.Name = "ProviderVerifyPayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderVerifyPayment) prepareRequest() bool {
	apiEndpoint := flow.Tenant.EndpointBe + "/pagamentoAttesoByIuv" + "?codiceEnte=" + flow.Tenant.Code + "&codiceServizio=" + flow.Service.Code + "&iuv=" + flow.Payment.Payment.IUV
	verifyReq, err := http.NewRequest("GET", apiEndpoint, bytes.NewReader([]byte{}))
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing verify request"
		return false
	}
	verifyReq.Header.Add("Content-Type", "application/json")

	flow.Request = verifyReq

	return true
}

func (flow *FlowProviderVerifyPayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_verify"))

	flow.Client = &http.Client{}
	if flow.Tenant.SslCrt != "" && flow.Tenant.SslKey != "" {
		crt, err := tls.X509KeyPair([]byte(flow.Tenant.SslCrt), []byte(flow.Tenant.SslKey))
		if err != nil {
			flow.Err = err
			flow.Msg = "errors while loading the ssl certificate"

			MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

			return false
		}

		tlsConfig := &tls.Config{
			Certificates: []tls.Certificate{crt},
		}

		tr := &http.Transport{
			TLSClientConfig: tlsConfig,
		}
		flow.Client.Transport = tr
	}

	verifyRes, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting verify request"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}
	defer verifyRes.Body.Close()
	verifyBody, err := io.ReadAll(verifyRes.Body)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while reading verify response"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	flow.Result = &models.SilfiVerifyResponse{}
	err = json.Unmarshal(verifyBody, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error while reading verify response"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	if flow.Result.Payment == nil {
		flow.Msg = "provider did not give us a verification result"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.Completed = !flow.Result.Payment.Payable
	flow.Payed = flow.Result.Payment.Paid
	if flow.Payed {
		flow.PaidAt = models.Time{Time: time.Now()}
	}

	return true
}
