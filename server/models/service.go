package models

type Service struct {
	ID          string         `json:"id"`
	TenantID    string         `json:"tenant_id"`
	Active      bool           `json:"active"`
	Code        string         `json:"code"`
	EntryCode   string         `json:"entry_code"`
	Description string         `json:"description"`
	AccEntry    string         `json:"acc_entry"`
	AccCap      string         `json:"acc_cap"`
	AccArt      string         `json:"acc_art"`
	Splitted    bool           `json:"splitted"`
	Split       []ServiceSplit `json:"split"`
}

type ServiceSplit struct {
	Code        string  `json:"split_code"`
	Amount      float64 `json:"split_amount" description:"..." format:"float"`
	Description string  `json:"split_description"`
	AccEntry    string  `json:"split_acc_entry"`
	AccCap      string  `json:"split_acc_cap"`
	AccArt      string  `json:"split_acc_art"`
}
