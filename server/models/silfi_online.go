package models

type SilfiOnlineRequest struct {
	TenantCode  string   `json:"codiceEnte"`     // tenant.code
	ServiceCode string   `json:"codiceServizio"` // service.code
	IUVs        []string `json:"iuvFeList"`
	LandingUrl  string   `json:"urlRitorno"`
}

type SilfiOnlineResponse struct {
	Token string `json:"token"`
}
