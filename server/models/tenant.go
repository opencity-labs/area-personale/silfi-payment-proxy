package models

type Tenant struct {
	ID         string `json:"id"`
	Active     bool   `json:"active"`
	Production bool   `json:"production"`
	Code       string `json:"code"`
	EndpointBe string `json:"endpoint_be"`
	EndpointFe string `json:"endpoint_fe"`
	SslCrt     string `json:"ssl_crt"`
	SslKey     string `json:"ssl_key"`
}
