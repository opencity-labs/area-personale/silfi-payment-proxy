package models

type Schema struct {
	Display    string            `json:"display,omitempty"`
	Components []SchemaComponent `json:"components,omitempty"`
}

type SchemaComponent struct {
	Label                  string                      `json:"label,omitempty"`
	Description            string                      `json:"description,omitempty"`
	Placeholder            string                      `json:"placeholder,omitempty"`
	Spellcheck             bool                        `json:"spellcheck,omitempty"`
	Attributes             *SchemaComponentAttribute   `json:"attributes,omitempty"`
	Validate               *SchemaComponentValidate    `json:"validate,omitempty"`
	Key                    string                      `json:"key,omitempty"`
	Type                   string                      `json:"type,omitempty"`
	Input                  bool                        `json:"input,omitempty"`
	Hidden                 bool                        `json:"hidden,omitempty"`
	DefaultValue           any                         `json:"defaultValue,omitempty"`
	TableView              bool                        `json:"tableView,omitempty"`
	ShowValidations        bool                        `json:"showValidations,omitempty"`
	Reorder                bool                        `json:"reorder,omitempty"`
	AddAnotherPosition     string                      `json:"addAnotherPosition,omitempty"`
	LayoutFixed            bool                        `json:"layoutFixed,omitempty"`
	EnableRowGroups        bool                        `json:"enableRowGroups,omitempty"`
	InitEmpty              bool                        `json:"initEmpty,omitempty"`
	Conditional            *SchemaComponentConditional `json:"conditional,omitempty"`
	Components             []*SchemaComponent          `json:"components,omitempty"`
	Mask                   bool                        `json:"mask,omitempty"`
	Delimiter              bool                        `json:"delimiter,omitempty"`
	RequireDecimal         bool                        `json:"requireDecimal,omitempty"`
	InputFormat            any                         `json:"inputFormat,omitempty"`
	TruncateMultipleSpaces bool                        `json:"truncateMultipleSpaces,omitempty"`
	Widget                 string                      `json:"widget,omitempty"`
	CalculateValue         string                      `json:"calculateValue,omitempty"`
	Data                   *SchemaComponentData        `json:"data,omitempty"`
}

type SchemaComponentAttribute struct {
	Readonly string `json:"readonly,omitempty"`
}

type SchemaComponentValidate struct {
	Required      bool   `json:"required,omitempty"`
	Custom        string `json:"custom,omitempty"`
	CustomMessage string `json:"customMessage,omitempty"`
}

type SchemaComponentConditional struct {
	Show bool   `json:"show,omitempty"`
	When string `json:"when,omitempty"`
	Eq   string `json:"eq,omitempty"`
}

type SchemaComponentDefaultValueSplit struct {
	SplitCode        string  `json:"split_code"`
	SplitAmount      float64 `json:"split_amount" description:"..." format:"float"`
	SplitDescription string  `json:"split_description"`
	SplitAccEntry    string  `json:"split_acc_entry"`
	SplitAccCap      string  `json:"split_acc_cap"`
	SplitAccArt      string  `json:"split_acc_art"`
}

type SchemaComponentData struct {
	Values []*SchemaComponentDataValue `json:"values,omitempty"`
}

type SchemaComponentDataValue struct {
	Label string `json:"label,omitempty"`
	Value string `json:"value,omitempty"`
}

var TenantSchema = Schema{
	Display: "form",
	Components: []SchemaComponent{
		{
			Label:       "UUID del Tenant",
			Hidden:      true,
			Placeholder: "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
			Spellcheck:  false,
			Attributes: &SchemaComponentAttribute{
				Readonly: "readonly",
			},
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "id",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:        "Abilitato",
			Key:          "active",
			Type:         "checkbox",
			Input:        true,
			Hidden:       true,
			DefaultValue: true,
			TableView:    false,
		},
		{
			Label:        "Ambiente di produzione",
			Placeholder:  "Flaggare questo campo per abilitare l'ambiente di produzione",
			Key:          "production",
			Type:         "checkbox",
			Input:        true,
			DefaultValue: false,
			TableView:    false,
		},
		{
			Label:       "Codice ente",
			Placeholder: "Codice riferimento ente",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "code",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:       "Endpoint backend Silfi",
			Placeholder: "Endpoint backend API assegnato da Silfi",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "endpoint_be",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:       "Endpoint frontend Silfi",
			Placeholder: "Endpoint frontend API assegnato da Silfi",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "endpoint_fe",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:        "Certificato SSL",
			Placeholder:  "Certificato SSL di sicurezza",
			Key:          "ssl_crt",
			Type:         "textarea",
			Input:        true,
			DefaultValue: "",
			TableView:    true,
			Validate: &SchemaComponentValidate{
				Custom:        "valid = (input !== '' && data.ssl_key !== '') || (input === '' && data.ssl_crt === '');",
				CustomMessage: "Devi compilare entrambi i campi Certificato SSL e Chiave privata SSL o lasciarli entrambi vuoti.",
			},
		},
		{
			Label:        "Chiave privata SSL",
			Placeholder:  "Chiave privata SSL di sicurezza",
			Key:          "ssl_key",
			Type:         "textarea",
			Input:        true,
			DefaultValue: "",
			TableView:    true,
			Validate: &SchemaComponentValidate{
				Custom:        "valid = (input !== '' && data.ssl_crt !== '') || (input === '' && data.ssl_key === '');",
				CustomMessage: "Devi compilare entrambi i campi Certificato SSL e Chiave privata SSL o lasciarli entrambi vuoti.",
			},
		},
		{
			Label:           "Salva",
			ShowValidations: false,
			Key:             "submit",
			Type:            "button",
			Input:           true,
			TableView:       false,
		},
	},
}

var ServiceSchema = Schema{
	Display: "form",
	Components: []SchemaComponent{
		{
			Label:       "UUID del Servizio",
			Hidden:      true,
			Placeholder: "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
			Spellcheck:  false,
			Attributes: &SchemaComponentAttribute{
				Readonly: "readonly",
			},
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "id",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:       "UUID del Tenant",
			Hidden:      true,
			Placeholder: "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
			Spellcheck:  false,
			Attributes: &SchemaComponentAttribute{
				Readonly: "readonly",
			},
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "tenant_id",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:        "Abilitato",
			Hidden:       true,
			DefaultValue: true,
			Key:          "active",
			Type:         "checkbox",
			Input:        true,
			TableView:    false,
		},
		{
			Label:       "Codice servizio",
			Placeholder: "pag_gen",
			Description: "Questo codice è fornito all'ente da Silfi",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "code",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:       "Dati specifici riscossione",
			Placeholder: "9/1234",
			Description: "Dati specifici riscossione forniti all'ente da Silfi",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "entry_code",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:       "Descrizione del servizio",
			Placeholder: "descrizione test",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "description",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:       "Codice entrata",
			Placeholder: "3445",
			Description: "Codice entrata",
			Key:         "acc_entry",
			Type:        "textfield",
			Input:       true,
			TableView:   true,
		},
		{
			Label:       "Codice capitolo",
			Placeholder: "34",
			Description: "Codice capitolo",
			Key:         "acc_cap",
			Type:        "textfield",
			Input:       true,
			TableView:   true,
		},
		{
			Label:       "Codice accertamento",
			Placeholder: "3",
			Description: "Codice accertamento",
			Key:         "acc_art",
			Type:        "textfield",
			Input:       true,
			TableView:   true,
		},
		{
			Label:        "Bilancio",
			DefaultValue: true,
			Key:          "splitted",
			Type:         "checkbox",
			Input:        true,
			TableView:    true,
		},
		{
			Label:              "Dettagli bilancio",
			Reorder:            false,
			AddAnotherPosition: "bottom",
			LayoutFixed:        false,
			EnableRowGroups:    false,
			InitEmpty:          true,
			Conditional: &SchemaComponentConditional{
				Show: true,
				When: "splitted",
				Eq:   "true",
			},
			DefaultValue: []*SchemaComponentDefaultValueSplit{
				{
					SplitCode:        "c_1",
					SplitAmount:      0,
					SplitDescription: "descrizione test",
					SplitAccEntry:    "0",
					SplitAccCap:      "0",
					SplitAccArt:      "0",
				},
			},
			Key:       "split",
			Type:      "datagrid",
			Input:     true,
			TableView: true,
			Components: []*SchemaComponent{
				{
					Label:       "Codice",
					Placeholder: "c_1",
					Description: "Identificativo univoco della voce di bilancio. Testo libero",
					Validate: &SchemaComponentValidate{
						Required: true,
					},
					Key:       "split_code",
					Type:      "textfield",
					Input:     true,
					TableView: true,
				},
				{
					Label:       "Importo",
					Placeholder: "16.00",
					Description: "Importo della voce di bilancio. NB: La somma degli importi delle voci DEVE equivalere all'importo totale",
					Validate: &SchemaComponentValidate{
						Required: true,
					},
					Mask:                   false,
					Delimiter:              false,
					RequireDecimal:         false,
					InputFormat:            "plain",
					TruncateMultipleSpaces: false,
					Key:                    "split_amount",
					Type:                   "number",
					Input:                  true,
					TableView:              true,
				},
				{
					Label:       "Descrizione voce",
					Placeholder: "descrizione test",
					Description: "Descrizione voce",
					Validate: &SchemaComponentValidate{
						Required: true,
					},
					Key:       "split_description",
					Type:      "textfield",
					Input:     true,
					TableView: true,
				},
				{
					Label:       "Codice entrata",
					Placeholder: "3445",
					Description: "Codice entrata",
					Key:         "split_acc_entry",
					Type:        "textfield",
					Input:       true,
					TableView:   true,
				},
				{
					Label:       "Codice capitolo",
					Placeholder: "34",
					Description: "Codice capitolo",
					Key:         "split_acc_cap",
					Type:        "textfield",
					Input:       true,
					TableView:   true,
				},
				{
					Label:       "Codice accertamento",
					Placeholder: "3",
					Description: "Codice accertamento",
					Key:         "split_acc_art",
					Type:        "textfield",
					Input:       true,
					TableView:   true,
				},
			},
		},
		{
			Label:          "hidden",
			CalculateValue: "if (!data.split || data.split == 'undefined') {\n  data.split = []\n} else if (typeof data.split==='object' && Object.keys(data.split).length === 0) {\n  data.split = [];\n}",
			Key:            "hidden",
			Type:           "hidden",
			Input:          true,
			TableView:      false,
		},
		{
			Label:           "Salva",
			ShowValidations: false,
			Key:             "submit",
			Type:            "button",
			Input:           true,
			TableView:       false,
		},
	},
}
