package models

type SilfiVerifyResponse struct {
	Payment *SilfiVerifyResponsePayment `json:"pagamentoAtteso"`
}

type SilfiVerifyResponsePayment struct {
	Payable bool `json:"pagabile"`
	Paid    bool `json:"pagato"`
}
