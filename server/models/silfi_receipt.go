package models

type SilfiReceiptResponse struct {
	IUV     string                       `json:"iuv"` // se presente si prende la ricevuta pdf
	Receipt *SilfiReceiptResponseReceipt `json:"ricevutaTelematica"`
}

type SilfiReceiptResponseReceipt struct {
	Attachment *SilfiReceiptResponseAttachment `json:"pdf"`
}

type SilfiReceiptResponseAttachment struct {
	Receipt string `json:"pdf"`
}
