package models

type SilfiDueRequest []*SilfiDueRequestElement

type SilfiDueRequestElement struct {
	Payment   *SilfiDueRequestPayment `json:"pagamentoAtteso"`
	EntryCode string                  `json:"datiSpecificiRiscossione"` // service.EntryCode
}

type SilfiDueRequestPayment struct {
	TenantCode                   string                  `json:"codiceEnte"`          // tenant.code
	ServiceCode                  string                  `json:"codiceServizio"`      // service.code
	PaymentId                    string                  `json:"idBoPagamentoAtteso"` // payment.id
	Reason                       string                  `json:"causale"`
	Amount                       float64                 `json:"importo"`
	Debits                       []*SilfiDueRequestDebit `json:"partiteDebitorie"`
	PayerType                    string                  `json:"anagNaturaGiuridica"` // "F" fisica o "G" giuridica
	PayerTaxIdentificationNumber string                  `json:"anagCfPiva"`
	PayerName                    string                  `json:"anagDenominazione"` // nome+cognome o ragione sociale
	PayerStreetName              string                  `json:"anagIndirizzo"`
	PayerBuildingNumber          string                  `json:"anagCivico"`
	PayerTownName                string                  `json:"anagLocalita"`
	PayerPostalCode              string                  `json:"anagCap"`
	PayerCountrySubdivision      string                  `json:"anagProvincia"`
	PayerCountry                 string                  `json:"anagNazione"`
	PayerEmail                   string                  `json:"anagEmail"`
	Visible                      bool                    `json:"visibileSol"` // sempre true
	CreatedAt                    string                  `json:"dataInizioValidita"`
	ExpireAt                     string                  `json:"dataScadenza"`
	PrintExpireAt                string                  `json:"dataScadenzaStampabile"`
}

type SilfiDueRequestDebit struct {
	DebitId     string                  `json:"idBoPartitaDebitoria"` // payment.id
	Amount      float64                 `json:"importo"`
	Description string                  `json:"descrizione"` // service.description
	Splits      []*SilfiDueRequestSplit `json:"articoliDebitori"`
}

type SilfiDueRequestSplit struct {
	Amount      float64 `json:"importo"`
	Description string  `json:"descrizione"`
	AccCap      string  `json:"codiceCapitolo"`
	AccEntry    *string `json:"codiceEntrata,omitempty"`
	AccArt      string  `json:"codiceAccertamento"`
}

type SilfiDuePagamentiAttesiResponse struct {
	Payments []*SilfiDueResponsePayment `json:"pagamentiAttesi"`
}

type SilfiDueMarcaDaBolloResponse struct {
	Esito string `json:"esito"`
	Token string `json:"token"`
	Iuv   string `json:"iuv"`
}

type SilfiDueResponsePayment struct {
	PaymentId string `json:"idBoPagamentoAtteso"`
	Result    string `json:"codiceEsito"`      // SUCCESS o altro in caso di errore
	Message   string `json:"descrizioneEsito"` // se SUCCESS è "Operazione eseguita correttamente"
	IUV       string `json:"iuv"`
}

type MarcaDaBolloPayment struct {
	CodiceEnte          string  `json:"codiceEnte"`
	Importo             float64 `json:"importo"`
	HashDocumento       string  `json:"hashDocumento"`
	AnagCfPiva          string  `json:"anagCfPiva"`
	AnagDenominazione   string  `json:"anagDenominazione"`
	AnagNaturaGiuridica string  `json:"anagNaturaGiuridica"`
	AnagProvincia       string  `json:"anagProvincia"`
	UrlRitorno          string  `json:"urlRitorno"`
	CodiceServizio      string  `json:"codiceServizio"`
}
