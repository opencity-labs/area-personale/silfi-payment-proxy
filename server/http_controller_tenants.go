package server

import (
	"context"

	uuid "github.com/satori/go.uuid"
	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
	"gitlab.com/opencontent/stanza-del-cittadino/silfi-payment-proxy/server/models"
)

func GetTenantSchema(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(_ context.Context, _ struct{}, output *models.Schema) error {
		*output = models.TenantSchema

		return nil
	})

	uc.SetTitle("Get Tenant Form Schema")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	// uc.SetExpectedErrors(status.Internal)

	return uc
}

type getTenantByIDInput struct {
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func GetTenantByID(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input getTenantByIDInput, output *models.Tenant) error {
		tenantsSync.RLock()
		defer tenantsSync.RUnlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		*output = *tenant

		return nil
	})

	uc.SetTitle("Get Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type createTenantInput struct {
	ID         string `json:"id" description:"..." required:"true"`
	Active     bool   `json:"active" description:"..." required:"true"`
	Production bool   `json:"production" description:"..." required:"true"`
	Code       string `json:"code" description:"..." required:"true"`
	EndpointBe string `json:"endpoint_be" description:"..." required:"true"`
	EndpointFe string `json:"endpoint_fe" description:"..." required:"true"`
	SslCrt     string `json:"ssl_crt" description:"..." required:"true"`
	SslKey     string `json:"ssl_key" description:"..." required:"true"`
}

func CreateTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input createTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		_, err := tenantsCache.Get(ctx, input.ID)

		if err == nil || err.Error() != "value not found in store" {
			return status.AlreadyExists
		}

		_, err = uuid.FromString(input.ID)
		if err != nil {
			return status.InvalidArgument
		}

		output.ID = input.ID
		output.Active = input.Active
		output.Production = input.Production
		output.Code = input.Code
		output.EndpointBe = input.EndpointBe
		output.EndpointFe = input.EndpointFe
		output.SslCrt = input.SslCrt
		output.SslKey = input.SslKey

		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Save Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type updateTenantInput struct {
	TenantId   string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ID         string `json:"id" description:"..." required:"true"`
	Active     bool   `json:"active" description:"..." required:"true"`
	Production bool   `json:"production" description:"..." required:"true"`
	Code       string `json:"code" description:"..." required:"true"`
	EndpointBe string `json:"endpoint_be" description:"..." required:"true"`
	EndpointFe string `json:"endpoint_fe" description:"..." required:"true"`
	SslCrt     string `json:"ssl_crt" description:"..." required:"true"`
	SslKey     string `json:"ssl_key" description:"..." required:"true"`
}

func UpdateTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input updateTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		if input.ID != tenant.ID {
			return status.InvalidArgument
		}

		output.ID = input.ID
		output.Active = input.Active
		output.Production = input.Production
		output.Code = input.Code
		output.EndpointBe = input.EndpointBe
		output.EndpointFe = input.EndpointFe
		output.SslCrt = input.SslCrt
		output.SslKey = input.SslKey

		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Update Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type patchTenantInput struct {
	TenantId   string  `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Active     *bool   `json:"active" description:"..."`
	Production *bool   `json:"production" description:"..."`
	Code       *string `json:"code" description:"..."`
	EndpointBe *string `json:"endpoint_be" description:"..."`
	EndpointFe *string `json:"endpoint_fe" description:"..."`
	SslCrt     *string `json:"ssl_crt" description:"..."`
	SslKey     *string `json:"ssl_key" description:"..."`
}

func PatchTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input patchTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		output.ID = tenant.ID

		if input.Active == nil {
			output.Active = tenant.Active
		} else {
			output.Active = *input.Active
		}
		if input.Production == nil {
			output.Production = tenant.Production
		} else {
			output.Production = *input.Production
		}
		if input.Code == nil {
			output.Code = tenant.Code
		} else {
			output.Code = *input.Code
		}
		if input.EndpointBe == nil {
			output.EndpointBe = tenant.EndpointBe
		} else {
			output.EndpointBe = *input.EndpointBe
		}
		if input.EndpointFe == nil {
			output.EndpointFe = tenant.EndpointFe
		} else {
			output.EndpointFe = *input.EndpointFe
		}
		if input.SslCrt == nil {
			output.SslCrt = tenant.SslCrt
		} else {
			output.SslCrt = *input.SslCrt
		}
		if input.SslKey == nil {
			output.SslKey = tenant.SslKey
		} else {
			output.SslKey = *input.SslKey
		}

		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Update Existing Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type disableTenantInput struct {
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func DisableTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input disableTenantInput, _ *struct{}) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		tenant.Active = false

		err = StoreTenant(sctx, tenant)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Disable Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}
