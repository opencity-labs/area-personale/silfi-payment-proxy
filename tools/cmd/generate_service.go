package cmd

import (
	"encoding/json"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"github.com/spf13/cobra"
	"gitlab.com/opencontent/stanza-del-cittadino/silfi-payment-proxy/server/models"
)

var generateServiceCmd = &cobra.Command{
	Use:   "service",
	Short: "A brief description",
	Long:  `A longer description.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Generated service json:")
		fmt.Println()

		tenant_id, err := cmd.Flags().GetString("tenant_id")
		if err != nil {
			panic(err)
		}
		code, err := cmd.Flags().GetString("code")
		if err != nil {
			panic(err)
		}
		entry_code, err := cmd.Flags().GetString("entry_code")
		if err != nil {
			panic(err)
		}
		description, err := cmd.Flags().GetString("description")
		if err != nil {
			panic(err)
		}
		acc_entry, err := cmd.Flags().GetString("acc_entry")
		if err != nil {
			panic(err)
		}
		acc_cap, err := cmd.Flags().GetString("acc_cap")
		if err != nil {
			panic(err)
		}
		acc_art, err := cmd.Flags().GetString("acc_art")
		if err != nil {
			panic(err)
		}
		split, err := cmd.Flags().GetBool("split")
		if err != nil {
			panic(err)
		}

		serviceSplits := []models.ServiceSplit{}
		if split {
			serviceSplits = []models.ServiceSplit{
				{
					Code:        "c_1",
					Amount:      16.00,
					Description: "Causale c_1",
					AccEntry:    "3445",
					AccCap:      "34",
					AccArt:      "3",
				},
				{
					Code:        "a_1",
					Amount:      0.50,
					Description: "Causale a_1",
					AccEntry:    "3445",
					AccCap:      "34",
					AccArt:      "3",
				},
			}
		}

		service := models.Service{
			ID:          uuid.NewV4().String(),
			TenantID:    tenant_id,
			Active:      true,
			Code:        code,
			EntryCode:   entry_code,
			Description: description,
			AccEntry:    acc_entry,
			AccCap:      acc_cap,
			AccArt:      acc_art,
			Splitted:    split,
			Split:       serviceSplits,
		}

		bytes, err := json.MarshalIndent(service, "", "  ")
		if err != nil {
			panic(err)
		}

		fmt.Println(string(bytes))
		fmt.Println()
	},
}

func init() {
	generateCmd.AddCommand(generateServiceCmd)

	generateServiceCmd.Flags().StringP("tenant_id", "t", "", "ID del tenant")
	generateServiceCmd.MarkFlagRequired("tenant_id")
	generateServiceCmd.Flags().StringP("code", "c", "pag_gen", "Codice servizio")
	generateServiceCmd.Flags().StringP("entry_code", "e", "9/1234", "Codice entrata")
	generateServiceCmd.Flags().StringP("description", "d", "descrizione test", "Descrizione del servizio")
	generateServiceCmd.Flags().StringP("acc_entry", "y", "3445", "Voce contabilità")
	generateServiceCmd.Flags().StringP("acc_cap", "p", "34", "Capitolo contabilità")
	generateServiceCmd.Flags().StringP("acc_art", "r", "3", "Articolo contabilità")
	generateServiceCmd.Flags().BoolP("split", "b", false, "Con bilancio")
}
