package cmd

import (
	"encoding/json"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"github.com/spf13/cobra"
	"gitlab.com/opencontent/stanza-del-cittadino/silfi-payment-proxy/server/models"
)

var generateTenantCmd = &cobra.Command{
	Use:   "tenant",
	Short: "A brief description",
	Long:  `A longer description.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Generated tenant json:")
		fmt.Println()

		production, err := cmd.Flags().GetBool("production")
		if err != nil {
			panic(err)
		}
		code, err := cmd.Flags().GetString("code")
		if err != nil {
			panic(err)
		}
		endpoint_be, err := cmd.Flags().GetString("endpoint_be")
		if err != nil {
			panic(err)
		}
		endpoint_fe, err := cmd.Flags().GetString("endpoint_fe")
		if err != nil {
			panic(err)
		}

		tenant := models.Tenant{
			ID:         uuid.NewV4().String(),
			Active:     true,
			Production: production,
			Code:       code,
			EndpointBe: endpoint_be,
			EndpointFe: endpoint_fe,
			SslCrt:     "-----BEGIN CERTIFICATE-----\nMIIDnzCCAoegAwIBAgIUUeXsJ1upEQirAi2Dx4frXzhOTzEwDQYJKoZIhvcNAQEL\nBQAwXzELMAkGA1UEBhMCSVQxEDAOBgNVBAgMB0ZpcmVuemUxEDAOBgNVBAcMB0Zp\ncmVuemUxFTATBgNVBAoMDEZpcmVuemVzbWFydDEVMBMGA1UEAwwMRmlyZW56ZXNt\nYXJ0MB4XDTIzMTEyMjEwMTc1NFoXDTMzMTExOTEwMTc1NFowXzELMAkGA1UEBhMC\nSVQxEDAOBgNVBAgMB0ZpcmVuemUxEDAOBgNVBAcMB0ZpcmVuemUxFTATBgNVBAoM\nDEZpcmVuemVzbWFydDEVMBMGA1UEAwwMRmlyZW56ZXNtYXJ0MIIBIjANBgkqhkiG\n9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhCPXXdgv3QA2nNkFs7tPAf475W9huYJIG+83\nQs5fxTXwRfKlmMoDsOJBu28t3DHTAdDjB+KGTwJBsIlYiYGqokmBoWJ+rWO0CNnb\nTx4tLyv2WNM/qFtdUL36AARZmRjvlsro/J8DhAz4LfTNRkbKQ53Z0RzjJOMhiO0S\nibnt/c9vU1iFAH9kamKDBvc9omh2pVhdJRQketMUSCTdcEEY7wsEe06mKB2/MYtb\nLRuEWypQZcoHgqvtzyslFbdERAM9IH80/eJjd46tvYeHrWRptMkhEtvYTJkvhK/V\nt0IwVBVAZrItRSP9PChFAUjjF1lbilWIht7xgDoe+rClyG0JnwIDAQABo1MwUTAd\nBgNVHQ4EFgQUcGElpRY6nqHyZh6Y4Ax7poSLM3IwHwYDVR0jBBgwFoAUcGElpRY6\nnqHyZh6Y4Ax7poSLM3IwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0BAQsFAAOC\nAQEALInrzcfA73USkhcCnfYJ5ZefUwHMVQ1Ylgr8AZeYuB9Ctjgsm72i+q0FT0as\nj93iZ11AURMQmZHjuUF5QPi4vmTz2iwp5owfO64Tfats/VxQGRP7LshBHj7yEjz3\neKfdytk0TJegZqOd0PN23u2HqR1A1ZPDGeJHKPLPjdSbUzBLyTF0g7S9HmPiiZsx\nvciDzFbfBSR9KVLs1BejTfFDjvsoOAEc97mmnMW78X8OZGUKuc9j2GNqG+j5Oe9j\nHg5AL27qldz0ZASf71WUqUG/0GNly4dXkD3Cl0bmlVAkPpjL4W9IyHnh55V6J/QK\nfNG0dNE7kRFW1ND5+cq3LvLsPg==\n-----END CERTIFICATE-----",
			SslKey:     "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCEI9dd2C/dADac\n2QWzu08B/jvlb2G5gkgb7zdCzl/FNfBF8qWYygOw4kG7by3cMdMB0OMH4oZPAkGw\niViJgaqiSYGhYn6tY7QI2dtPHi0vK/ZY0z+oW11QvfoABFmZGO+Wyuj8nwOEDPgt\n9M1GRspDndnRHOMk4yGI7RKJue39z29TWIUAf2RqYoMG9z2iaHalWF0lFCR60xRI\nJN1wQRjvCwR7TqYoHb8xi1stG4RbKlBlygeCq+3PKyUVt0REAz0gfzT94mN3jq29\nh4etZGm0ySES29hMmS+Er9W3QjBUFUBmsi1FI/08KEUBSOMXWVuKVYiG3vGAOh76\nsKXIbQmfAgMBAAECggEADRzE+2YZApkWhXdc3MpBAevsnXNXvLTxAXvTvX7e7qID\nS985gurE9ZgC7Gqqn9eX3m3DVNFFmtbllgzDBH4hl97e2X9j6tvI2tOXf7qsXCp3\nuxyKjBY1sW3wpKP9EIQ/cVserJBkJNK1C+zNzX2RbUB5A72vzCv3wQkmPK5LfGyL\ni4qfGKgB5ea72zVlO+PpXVfhmej4aiTrAZ0BZLbgfpRYWB01iX85gpjPzABuqlJr\nZz46Cd4EQD/MiYFkK8J16v12Eay/Ch+HAGPRCBFdDSZE7NGnuh/f0kR7JGOiA8b4\n0T9qi7CCSsgO3++3H4KUm1XrVvrnGGHIdudFkSlUlQKBgQC6cuvNdFF25v6TOrsi\ndNgRPT+1+ziUlIx4nlQIMl5+mGqIEXlleFBE+Fnmrb0P2oJxmOzMZmE7slCfSbL0\nO1MggzIhzDSKwN09NNjp0YtXwDJlOBerpxakiTg9bHFTYXVwblrpwLIU8PEx6uu5\nZMEjE46dNMw/qKYX/KhOcIF/swKBgQC1bqaAtadhm2jPzPD6IsjCmqqOZUeZ7tge\nD7buR+KMJCryOrsr7fT7xobvg/IoCuGdAPVJj77+cCm2h7nY9TaH87II+TzLcOut\nt7newYRiXnWL5qm/QsmODRQs2gJ3XvDFF8Wvi6WC3wgCC+/GK8lOvmRe7z4VVzsP\nuVL6Sb+4ZQKBgBA4EQUlWjTSsVCTalX7/hO51UH6YZijN3kCpOK1pvyyO4osovfN\nuIXXaNKlSG4JnxFMsUSyn0C3KUZB/fw68OdCImlIP0XWsygw4bGtVcbDZF7ju0Nm\nXJThju/g8BbFwpaOMwBkj9k8gG7MNcdB2X84o05PmnjrLc+mXem85bb7AoGAORZI\n46TEtuycbQ4SINKDEuBxm/54KhJCBCtzpsQNn4Td0nvyWt8zv2j3GjQNJNZPqQQd\nKflF4m9OYLWSH5cAgoSGV1Z875JnRbDSlimTyxPnro50iM6t9rlcyMQMFJ1V6t4R\nAKxS9rvQnDw/RL6QR+uWT1y9zx80YT0xw7kizxUCgYEArf+4ZqHa2quetECr1PiE\n+sZnfR+z6H1MqG5BZAb6eS38rV1n1x4Op+/2X4H8VXpLFlBRpOmbkBH854VOhvH5\n7Z4pAeuglRvieXwpywHuB+U/3aHe2L+RjB3cNSxMk6r/z4NcwTvcZ3HziXTtHxj/\nK/2/QzannLTinEAaHHNfjDE=\n-----END PRIVATE KEY-----",
		}

		bytes, err := json.MarshalIndent(tenant, "", "  ")
		if err != nil {
			panic(err)
		}

		fmt.Println(string(bytes))
		fmt.Println()
	},
}

func init() {
	generateCmd.AddCommand(generateTenantCmd)

	generateTenantCmd.Flags().Bool("production", false, "Modalità production?")
	generateTenantCmd.Flags().StringP("code", "c", "FIRENZE", "Codice riferimento del tenant")
	generateTenantCmd.Flags().StringP("endpoint_be", "e", "https://pagopabe-staging.055055.it/pagopabe/api-rest/", "EndpointBe del tenant")
	generateTenantCmd.Flags().StringP("endpoint_fe", "f", "https://pagopa-staging.055055.it/pagopafe/fe", "EndpointFe del tenant")
}
